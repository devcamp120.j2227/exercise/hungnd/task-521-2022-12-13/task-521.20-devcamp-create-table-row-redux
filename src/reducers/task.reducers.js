import { TASK_ADD_ROW_CLICK, TASK_INPUT_CHANGE } from "../constants/task.constants";

const initialState = {
    input:"",
    taskTable: []
}

const taskReducer =  (state = initialState, action) =>{
    switch(action.type){
        case TASK_INPUT_CHANGE:
            state.input = action.payload;
            break;
        case TASK_ADD_ROW_CLICK:
            state.taskTable.push({
                id: state.taskTable.length,
                name: state.input
            })
            state.input ="";
            break;
       default: 
            break;
    }
    return {...state} ;
}

export default taskReducer;